express = require("express");

var app = express();

app.get("/results/:n1/:n2",function (req, res) {
    res.send((Number(req.params["n1"])+Number(req.params["n2"])).toString());
});

app.post("/results/:n1/:n2",function (req, res) {
    res.send((Number(req.params["n1"])*Number(req.params["n2"])).toString());
});

app.put("/results/:n1/:n2",function (req, res) {
    res.send((Number(req.params["n1"])/Number(req.params["n2"])).toString());
});
app.patch("/results/:n1/:n2",function (req, res) {
    res.send((Number(req.params["n1"])**Number(req.params["n2"])).toString());
});
app.delete("/results/:n1/:n2",function (req, res) {
    res.send((Number(req.params["n1"])-Number(req.params["n2"])).toString());
});

app.listen(9876);